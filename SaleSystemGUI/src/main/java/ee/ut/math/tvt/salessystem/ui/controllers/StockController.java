package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class StockController implements Initializable {
    private static final Logger log = LogManager.getLogger(StockController.class);
    private final SalesSystemDAO dao;

    @FXML
    private Button addItemButton;
    @FXML
    private Button refreshWarehouseButton;
    @FXML
    private TextField barcodeField;
    @FXML
    private TextField amountField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private TableView<StockItem> warehouseTableView;

    public StockController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();
        // TODO refresh view after adding new items
    }

    @FXML
    public void refreshWarehouseButtonClicked() {
        refreshStockItems();
    }

    private void refreshStockItems() {
        log.info("Stock items refreshed");
        warehouseTableView.setItems(FXCollections.observableList(dao.getAllStockItems()));
        warehouseTableView.refresh();
    }

    // Adding items
    @FXML
    public void addItemButtonClicked() {
        // Create the item
        try {
            log.debug("User input barcodeField: " + barcodeField.getText());
            log.debug("User input amountField: " + amountField.getText());
            log.debug("User input nameField: " + nameField.getText());
            log.debug("User input priceField: " + priceField.getText());
            StockItem newItem = new StockItem();
            newItem.setId(Long.parseLong(barcodeField.getText()));
            newItem.setQuantity(Integer.parseInt(amountField.getText()));
            newItem.setName(nameField.getText());
            newItem.setPrice(Integer.parseInt(priceField.getText()));
            dao.saveStockItem(newItem);
            refreshStockItems();
            log.info("Item added to stock");
        } catch (NumberFormatException e) {
            log.info("Item was not added to stock (wrong input)");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("WRONG INPUT!");
            alert.setContentText("Oooopsie, you entered something wrong. Try again.");
            alert.showAndWait();
        } catch (IllegalArgumentException e) {
            log.info(e);
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("WRONG INPUT!");
            alert.setContentText("Negative input not allowed");
            alert.showAndWait();
        }
    }
}
