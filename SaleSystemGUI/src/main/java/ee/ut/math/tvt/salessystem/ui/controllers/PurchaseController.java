package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class PurchaseController implements Initializable {

    private static final Logger log = LogManager.getLogger(PurchaseController.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;

    @FXML
    private ComboBox<StockItem> ProductsDropdownMenu;
    @FXML
    private Button newPurchase;
    @FXML
    private Button submitPurchase;
    @FXML
    private Button cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItemButton;
    @FXML
    private TableView<SoldItem> purchaseTableView;
    @FXML
    private TitledPane shoppingCartTitlePane; // We will use the title pane header for the total sum

    public PurchaseController(SalesSystemDAO dao, ShoppingCart shoppingCart) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        ProductsDropdownMenu.setDisable(true);
        addDropdownItems();
        purchaseTableView.setItems(FXCollections.observableList(shoppingCart.getAll()));
        disableProductField(true);

        this.barCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem();
                }
            }
        });
        setSum(0);
    }

    @FXML
    public void fillInputs() {
        StockItem selectedItem = ProductsDropdownMenu.getValue();
        barCodeField.setText(String.valueOf(selectedItem.getId()));
        nameField.setText(selectedItem.getName());
        priceField.setText(String.valueOf(selectedItem.getPrice()));
    }

    public void addDropdownItems() {
        log.info("Adding items to the dropdown menu");
        ProductsDropdownMenu.getItems().addAll(FXCollections.observableList(dao.getAllStockItems()));
    }

    public void updateDropdownMenu() {
        log.debug("updating dropdown menu");
        ObservableList<StockItem> products = FXCollections.observableList(dao.getAllStockItems());
        ObservableList<StockItem> menuitems = ProductsDropdownMenu.getItems();
        for (StockItem product : products) {
            if (!menuitems.contains(product)) {
                ProductsDropdownMenu.getItems().add(product);
            }
        }
    }

    public void setSum(double total) {
        shoppingCartTitlePane.setText("Total: " + total + " Euro.");
    }

    /**
     * Event handler for the <code>new purchase</code> event.
     */
    @FXML
    protected void newPurchaseButtonClicked() {
        log.info("New sale process started");
        try {
            enableInputs();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    protected void cancelPurchaseButtonClicked() {
        log.info("Sale cancelled");
        try {
            shoppingCart.cancelCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
            setSum(0);
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     */
    @FXML
    protected void submitPurchaseButtonClicked() {
        log.info("Sale complete");
        try {
            log.debug("Contents of the current basket:\n" + shoppingCart.getAll());
            shoppingCart.submitCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
            setSum(0);

        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    // switch UI to the state that allows to proceed with the purchase
    private void enableInputs() {
        resetProductField();
        disableProductField(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
        ProductsDropdownMenu.setDisable(false);
    }

    // switch UI to the state that allows to initiate new purchase
    private void disableInputs() {
        resetProductField();
        ProductsDropdownMenu.setDisable(true);
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
    }

    private void fillInputsBySelectedStockItem() {
        StockItem stockItem = dao.getStockItemUsingID(Long.parseLong(barCodeField.getText()));
        if (stockItem != null) {
            nameField.setText(stockItem.getName());
            priceField.setText(String.valueOf(stockItem.getPrice() / 100.0));
        } else {
            log.debug("stockItem null");
            resetProductField();
        }
    }

    /**
     * Add new item to the cart.
     */
    @FXML
    public void addItemEventHandler() {
        // add chosen item to the shopping cart.
        try {
            StockItem stockItem = dao.getStockItemUsingID(Long.parseLong(barCodeField.getText()));
            if (stockItem != null) {
                int quantity = Integer.parseInt(quantityField.getText());
                shoppingCart.addItem(new SoldItem(stockItem, quantity));
                purchaseTableView.refresh();
                setSum(shoppingCart.calculateTotalInEuro()); // format it in eur, not cent
            }
        } catch (SalesSystemException e) {
            log.error(e);
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("Not enough items in stock");
            alert.showAndWait();
        } catch (NumberFormatException e) {
            log.error(e);
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("Malformed amount of item");
            alert.showAndWait();
        }
    }

    /**
     * Sets whether or not the product component is enabled.
     */
    private void disableProductField(boolean disable) {
        this.addItemButton.setDisable(disable);
        this.barCodeField.setDisable(true);
        this.quantityField.setDisable(disable);
        this.nameField.setDisable(true);
        this.priceField.setDisable(true);
    }

    /**
     * Reset dialog fields.
     */
    private void resetProductField() {
        barCodeField.setText("");
        quantityField.setText("1");
        nameField.setText("");
        priceField.setText("");
    }
}
