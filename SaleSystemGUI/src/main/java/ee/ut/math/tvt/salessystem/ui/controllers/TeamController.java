package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.logic.PropertiesLoader;
import ee.ut.math.tvt.salessystem.ui.SalesSystemUI;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class TeamController implements Initializable {
    private static final Logger log = LogManager.getLogger(TeamController.class);
    @FXML private Label teamNameLabel;
    @FXML private Label member1Label;
    @FXML private Label member2Label;
    @FXML private Label member3Label;
    @FXML private Label contactPersonLabel;
    @FXML private ImageView teamImage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("TeamController initialized");
        Properties prop = PropertiesLoader.loadProperties();
        this.teamNameLabel.setText(prop.getProperty("teamName"));
        this.member1Label.setText(prop.getProperty("member1"));
        this.member2Label.setText(prop.getProperty("member2"));
        this.member3Label.setText(prop.getProperty("member3"));
        this.contactPersonLabel.setText("Contact: " + prop.getProperty("contactPerson"));
        this.teamImage.setImage(new Image(prop.getProperty("teamImage")));
    }
}