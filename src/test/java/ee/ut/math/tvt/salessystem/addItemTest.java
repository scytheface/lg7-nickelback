package ee.ut.math.tvt.salessystem;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class addItemTest {
    @Test
    public void testAddingItemWithNegativeQuantity() {
        StockItem newItem = new StockItem();
        assertThrows(IllegalArgumentException.class, () -> {
            newItem.setQuantity(-4);
        });
    }

    @Test
    public void testAddingItemBeginsAndCommitsTransaction(){

    }

    @Test
    public void testAddingNewItem(){
        Long id = Long.parseLong("999999");
        StockItem newItem = new StockItem(id,"TestItem","TestItem",100,100);
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        dao.saveStockItem(newItem);
        assertEquals("Item must be saved",newItem,dao.getStockItemUsingID(id));
    }

    @Test
    public void testAddingExistingItem(){
        // Adding an item to the warehouse
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        Long id = Long.parseLong("9999");
        StockItem existingItem = new StockItem(id,"TestItem","TestItem",100,100);
        dao.saveStockItem(existingItem);
        int existingQuantity = existingItem.getQuantity();
        // Adding the same item
        int addedQuantity = 40;
        StockItem newItem = new StockItem(id,"TestItem","TestItem",100,addedQuantity);
        dao.saveStockItem(newItem);
        System.out.println(dao.getStockItemUsingID(id).getQuantity());
        assertEquals("Quantity is increased",existingQuantity+addedQuantity,dao.getStockItemUsingID(id).getQuantity());
    }

}
