package ee.ut.math.tvt.salessystem;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class ShoppingCartAddItem {

    @Test
    public void testAddingExistingItem(){
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart shoppingCart = new ShoppingCart(dao);
        Long id = Long.parseLong("9999");
        StockItem stockitem = new StockItem(id,"TestItem","TestItem",100,100);
        SoldItem soldItem1 = new SoldItem(stockitem,10);
        SoldItem soldItem2 = new SoldItem(stockitem,5);
        shoppingCart.addItem(soldItem1);
        shoppingCart.addItem(soldItem2);

        List<SoldItem> cartItems = shoppingCart.getAll();
        int newQuantity = 0;
        for (int i = 0; i < cartItems.size(); i++) {
            if(cartItems.get(i).getStockItem().getId().equals(stockitem.getId())){
                newQuantity = cartItems.get(i).getQuantity();
                break;
            }
        }
        assertEquals("Quantity is increased",15,newQuantity);
    }

    @Test
    public void testAddingNewItem(){
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart shoppingCart = new ShoppingCart(dao);
        Long id = Long.parseLong("9999");
        StockItem stockitem = new StockItem(id,"TestItem","TestItem",100,100);
        SoldItem soldItem1 = new SoldItem(stockitem,10);
        shoppingCart.addItem(soldItem1);
        assertEquals("Item was added to shopping cart",soldItem1.getStockItem(),shoppingCart.getAll().get(0).getStockItem());
    }

    @Test
    public void testAddingItemWithQuantityTooLarge(){
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart shoppingCart = new ShoppingCart(dao);
        Long id = Long.parseLong("9999");
        StockItem stockitem = new StockItem(id,"TestItem","TestItem",100,1);
        SoldItem soldItem1 = new SoldItem(stockitem,100);
        assertThrows(SalesSystemException.class, () -> {
            shoppingCart.addItem(soldItem1);
        });
    }

    @Test
    public void testAddingItemWithNegativeQuantity(){
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart shoppingCart = new ShoppingCart(dao);
        Long id = Long.parseLong("9999");
        StockItem stockitem = new StockItem(id,"TestItem","TestItem",100,1);
        assertThrows(IllegalArgumentException.class, () -> {
            SoldItem soldItem1 = new SoldItem(stockitem,-10);
            shoppingCart.addItem(soldItem1);
        });
    }

    @Test
    public void testAddingItemWithQuantitySumTooLarge(){
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart shoppingCart = new ShoppingCart(dao);
        Long id = Long.parseLong("9999");
        StockItem stockitem = new StockItem(id,"TestItem","TestItem",100,10);
        assertThrows(SalesSystemException.class, () -> {
            SoldItem soldItem1 = new SoldItem(stockitem,9);
            shoppingCart.addItem(soldItem1);
            SoldItem soldItem2 = new SoldItem(stockitem,10);
            shoppingCart.addItem(soldItem2);
        });
    }
}
