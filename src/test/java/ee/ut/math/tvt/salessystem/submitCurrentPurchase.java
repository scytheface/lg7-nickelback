package ee.ut.math.tvt.salessystem;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.junit.Test;

import static org.junit.Assert.*;

public class submitCurrentPurchase {

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart shoppingCart = new ShoppingCart(dao);
        Long id = Long.parseLong("9999");
        StockItem existingItem = new StockItem(id, "TestItem", "TestItem", 100, 100);
        dao.saveStockItem(existingItem);
        // Purchase 5 existingItem
        SoldItem soldItem = new SoldItem(existingItem, 5);
        shoppingCart.addItem(soldItem);
        shoppingCart.submitCurrentPurchase();
        assertEquals("Purchasing item decreased item quantity in warehouse", 95, dao.getStockItemUsingID(id).getQuantity());
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart shoppingCart = new ShoppingCart(dao);
        assertEquals(0, dao.getCalledMethods().size());
        shoppingCart.submitCurrentPurchase();
        assertEquals(2, dao.getCalledMethods().size());
        assertEquals("begin", dao.getCalledMethods().get(0));
        assertEquals("commit", dao.getCalledMethods().get(1));
    }

    @Test
    public void testSubmittingCurrentOrderAddsSolditem() {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart shoppingCart = new ShoppingCart(dao);
        Long id = Long.parseLong("9999");
        StockItem existingItem = new StockItem(id, "TestItem", "TestItem", 100, 100);
        dao.saveStockItem(existingItem);
        SoldItem soldItem = new SoldItem(existingItem, 5);
        shoppingCart.addItem(soldItem);
        shoppingCart.submitCurrentPurchase();
        assertTrue("Submitting current order adds SoldItem", dao.getSoldItemList().contains(soldItem));
    }

    @Test
    public void testCancellingOrder() {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart shoppingCart = new ShoppingCart(dao);
        Long id = Long.parseLong("9999");
        StockItem existingItem1 = new StockItem(id, "Milk", "TestItem", 100, 100);
        StockItem existingItem2 = new StockItem(id, "Honey", "TestItem", 100, 100);
        dao.saveStockItem(existingItem1);
        dao.saveStockItem(existingItem2);
        SoldItem soldItem1 = new SoldItem(existingItem1, 5);
        SoldItem soldItem2 = new SoldItem(existingItem1, 5);

        // Add first item to shopping cart and cancel order and add second item to cart and purchase it
        shoppingCart.addItem(soldItem1);
        shoppingCart.cancelCurrentPurchase();
        shoppingCart.addItem(soldItem2);
        shoppingCart.submitCurrentPurchase();
        // Check that only second item got purchased
        assertFalse("First item was not purchased", dao.getSoldItemList().contains(soldItem1));
        assertTrue("Second item was purchased", dao.getSoldItemList().contains(soldItem2));
    }

    @Test
    public void testCancellingOrderQuanititesUnchanged() {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart shoppingCart = new ShoppingCart(dao);
        Long id = Long.parseLong("9999");
        StockItem existingItem = new StockItem(id, "Milk", "TestItem", 100, 100);
        dao.saveStockItem(existingItem);
        shoppingCart.addItem(new SoldItem(existingItem, 5));
        shoppingCart.cancelCurrentPurchase();
        assertEquals("Quantities unchanged after cancelling purchase", existingItem.getQuantity(), dao.getStockItemUsingID(id).getQuantity());
    }
}
