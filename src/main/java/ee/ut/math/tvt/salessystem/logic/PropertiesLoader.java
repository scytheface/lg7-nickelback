package ee.ut.math.tvt.salessystem.logic;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class PropertiesLoader {
    /**
     * Do not instantiate.
     */
    public PropertiesLoader() {
    }

    /** Loads properties for team info. Avoid reusing code in several places.
     * @return Properties file for use in team information.
     */
    public static Properties loadProperties() {
        Properties prop = new Properties();
        Path propFile = Paths.get("../src/main/resources/application.properties");
        try {
            prop.load(Files.newBufferedReader(propFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }
}
