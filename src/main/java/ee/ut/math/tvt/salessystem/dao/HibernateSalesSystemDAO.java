package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.hibernate.Hibernate;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;
    private List<String> calledMethods = new ArrayList<>();

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
        Configuration configuration = new org.hibernate.cfg.Configuration();
        //configuration.addAnnotatedClass(StockItem.class);
        configuration.addAnnotatedClass(SoldItem.class);
    }

    // TODO implement missing methods

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public List<SoldItem> getSoldItemList() {
        return em.createQuery("from SoldItem", SoldItem.class).getResultList();
    }

    @Override
    public List<StockItem> getAllStockItems() {
        return em.createQuery("from StockItem", StockItem.class).getResultList();
    }

    @Override
    public StockItem getStockItemUsingID(long id) {
        return em.find(StockItem.class,id);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        beginTransaction();
        StockItem detached = em.find(StockItem.class,stockItem.getId());
        if(detached == null){ // Then it is a new item
            em.persist(stockItem);
        }
        else{ // Stockitem exist in the DB so we add the quantities together
            detached.setQuantity(detached.getQuantity()+stockItem.getQuantity());
            em.merge(detached);
        }
        commitTransaction();
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        em.merge(item);
    }

    @Override
    public void beginTransaction() {
        calledMethods.add("begin");
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        calledMethods.add("rollback");
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        calledMethods.add("commit");
        em.getTransaction().commit();
    }
    @Override
    public List<String> getCalledMethods() {
        return calledMethods;
    }
}