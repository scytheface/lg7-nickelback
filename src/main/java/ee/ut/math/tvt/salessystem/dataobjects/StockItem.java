package ee.ut.math.tvt.salessystem.dataobjects;

import ee.ut.math.tvt.salessystem.SalesSystemException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Stock item.
 */

@Entity
@Table(name = "StockItem")
public class StockItem {

    @Id
    private Long id;
    @Column(name ="NAME")
    private String name;
    @Column(name = "PRICE")
    private Integer price;
    @Column(name="DESCRIPTION")
    private String description;
    @Column(name = "QUANTITY")
    private int quantity;

    public StockItem() {
    }

    public StockItem(Long id, String name, String desc, int price, int quantity) {
        if (id < 0 || price < 0 || quantity < 0) {
            throw new IllegalArgumentException("Negative input not allowed");
        }
        this.id = id;
        this.name = name;
        this.description = desc;
        this.price = price;
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        if(price < 0){throw new IllegalArgumentException("Negative input not allowed");}
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        if(id < 0){throw new IllegalArgumentException("Negative input not allowed");}
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        if(quantity < 0){throw new IllegalArgumentException("Negative input not allowed");}
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return name;
        //return String.format("StockItem{id=%d, name='%s'}", id, name);
    }
}
