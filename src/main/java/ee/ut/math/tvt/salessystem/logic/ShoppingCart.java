package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {
        if (item.getStockItem().getQuantity() - item.getQuantity() < 0) {
            throw new SalesSystemException("Not enough items in stock");
        }

        Long itemId = item.getStockItem().getId();
        boolean exist = false;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getStockItem().getId().equals(itemId)) {
                if (item.getStockItem().getQuantity() - (items.get(i).getQuantity() + item.getQuantity()) < 0) {
                    throw new SalesSystemException("Not enough items in stock");
                }
                items.get(i).setQuantity(items.get(i).getQuantity() + item.getQuantity());
                exist = true;
                break;
            }
        }

        if (!exist) {
            items.add(item);
        }
        //log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        items.clear();
    }

    public void submitCurrentPurchase() {
        // decrease quantities of the warehouse stock
        for (SoldItem soldItem : items) {
            soldItem.getStockItem().setQuantity((soldItem.getStockItem().getQuantity())-(soldItem.getQuantity()));
        }
        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        dao.beginTransaction();
        try {
            for (SoldItem item : items) {
                dao.saveSoldItem(item);
            }
            dao.commitTransaction();
            items.clear();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

    public int calculateTotal() {
        int total = 0;
        for (SoldItem item : items) {
            total += item.getSum();
        }
        return total;
    }

    public double calculateTotalInEuro() {
        return calculateTotal() / 100.0;
    }
}
