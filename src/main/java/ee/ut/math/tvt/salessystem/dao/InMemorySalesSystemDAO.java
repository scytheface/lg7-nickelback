package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.ArrayList;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private List<String> calledMethods = new ArrayList<>();

    public List<SoldItem> getSoldItemList(){
        return soldItemList;
    }
    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 200, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 80, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sausages", 220, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
    }

    @Override
    public List<StockItem> getAllStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem getStockItemUsingID(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        // Make sure we do not have the same item already.
        StockItem modify = null; // The item we will change if we already have it.

        // Extremely highly advanced linear search... for now...
        // And by for now I mean forever, of course.
        for (StockItem item: stockItemList) {
            if (item.getId().equals(stockItem.getId())) {
                modify = item;
                break;
            }
        }
        if (modify == null) {
            // If we did not have the item, we can simply add it.
            stockItemList.add(stockItem);
        } else {
            // If we did have the item, we must modify it.
            modify.setName(stockItem.getName());
            modify.setPrice(stockItem.getPrice());
            modify.setQuantity(modify.getQuantity() + stockItem.getQuantity());
        }
    }


    @Override
    public void beginTransaction() {
        calledMethods.add("begin");
    }

    @Override
    public void rollbackTransaction() {
        calledMethods.add("rollback");
    }

    @Override
    public void commitTransaction() {
        calledMethods.add("commit");
    }

    public List<String> getCalledMethods() {
        return calledMethods;
    }
}
