# Team LG7-NICKELBACK
1. Tõnis Hendrik Hlebnikov
2. Raido Everest
3. Risto Rahulan

## Homework 1:
  * [Requirements Gathering](https://bitbucket.org/scytheface/lg7-nickelback/wiki/Homework%201%20-%20Requirements%20Gathering)

## Homework 2:
  * [Requirements Specification, Modeling, Planning](https://bitbucket.org/scytheface/lg7-nickelback/wiki/Homework%202%20-%20Requirements%20Specification,%20Modeling,%20Planning)
  * [Issue tracker](https://bitbucket.org/scytheface/lg7-nickelback/issues?status=new&status=open)
  
## Homework 3:
  * [Commits for tag homework-3](https://bitbucket.org/scytheface/lg7-nickelback/commits/tag/homework-3)

## Homework 4:
  * [Debug](https://bitbucket.org/scytheface/lg7-nickelback/wiki/Homework%204%20-%20Debug%20Task)
  * [Commits for tag homework-4](https://bitbucket.org/scytheface/lg7-nickelback/commits/tag/homework-4)

## Homework 5:
* [Commits for tag homework-5](https://bitbucket.org/scytheface/lg7-nickelback/commits/tag/homework-5)

## Homework 6:
* [Commits for tag homework-6](https://bitbucket.org/scytheface/lg7-nickelback/commits/tag/homework-6)
* [System-level functional test plan](https://bitbucket.org/scytheface/lg7-nickelback/wiki/Homework%206%20(Task%203)%20-%20System-level%20functional%20test%20plan)

## Homework 7:
* [Commits for tag homework-7](https://bitbucket.org/scytheface/lg7-nickelback/commits/tag/homework-7)
* [Homework 7 - Task 1](https://bitbucket.org/scytheface/lg7-nickelback/wiki/Homework%207%20-%20Task%201)
* [Homework 7 - Task 2.1](https://bitbucket.org/scytheface/lg7-nickelback/wiki/Homework%207%20-%20Task%202.1)
* [Homework 7 - Task 2.2](https://bitbucket.org/scytheface/lg7-nickelback/wiki/Homework%207%20-%20Task%202.2)