package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.PropertiesLoader;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart cart;

    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
    }

    public static void main(String[] args) throws Exception {
        SalesSystemDAO dao = new HibernateSalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        log.info("Sales system CLI started.");
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }
    }

    private void showStock() {
        log.info("Showing stock");
        List<StockItem> stockItems = dao.getAllStockItems();
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() / 100.0 + "Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showCart() {
        log.info("Showing cart");
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si.getName() + " " + si.getPrice() / 100.0 + "Euro (" + si.getQuantity() + " items)");
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("Total: " + cart.calculateTotalInEuro() + " Euro");
        System.out.println("-------------------------");
    }

    private void showHistory(){
        log.info("Showing history");
        List<SoldItem> soldItems = dao.getSoldItemList();
        System.out.println("------------------------");
        for (SoldItem soldItem : soldItems) {
            System.out.println(soldItem.getName()+" "+soldItem.getPrice()+" "+soldItem.getQuantity());
        }
        if (soldItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void printTeamInfo(){
        log.info("Showing team info.");
        Properties prop = PropertiesLoader.loadProperties();
        System.out.println("-------------------------");
        System.out.println("Team "+prop.getProperty("teamName"));
        System.out.println("Team members: ");
        System.out.println(prop.getProperty("member1"));
        System.out.println(prop.getProperty("member2"));
        System.out.println(prop.getProperty("member3"));
        System.out.println("Contact person: "+prop.getProperty("contactPerson"));
        System.out.println("-------------------------");
    }

    private void printUsage() {
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("history\t\tShow history");
        System.out.println("n IDX NR N C\t Add NR items with index IDX, cost C and name N to the warehouse");

        System.out.println("c\t\tShow cart contents");
        System.out.println("a IDX NR \tAdd NR of stock item with index IDX to the cart");


        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("t\t\tShow team info");
        System.out.println("-------------------------");
    }

    private void processCommand(String command) {
        String[] c = command.split(" ");

        if (c[0].equals("h")) // help menu
            printUsage();
        else if (c[0].equals("q")) // terminate
            System.exit(0);
        else if (c[0].equals("w")) // warehouse contents
            showStock();
        else if(c[0].equals("history")) // History
            showHistory();
        else if (c[0].equals("c")) // cart contents
            showCart();
        else if (c[0].equals("p")) // purchase cart
            cart.submitCurrentPurchase();
        else if (c[0].equals("r")) // reset cart
            cart.cancelCurrentPurchase();
        else if (c[0].equals("t")) // team info
            printTeamInfo();
        else if (c[0].equals("a") && c.length == 3) { // adding item to shopping cart
            try {
                long idx = Long.parseLong(c[1]);
                int amount = Integer.parseInt(c[2]);
                StockItem item = dao.getStockItemUsingID(idx);
                if (item != null) {
                    cart.addItem(new SoldItem(item, Math.min(amount, item.getQuantity())));
                    System.out.println("New total: " + cart.calculateTotalInEuro() + " Euro.");
                } else {
                    System.out.println("no stock item with id " + idx);
                }
            } catch (SalesSystemException | NoSuchElementException | NumberFormatException e) {
                log.error(e.getMessage(), e);
            }
        } else if (c[0].equals("n") && c.length == 5) { // adding item to warehouse
            // IDX NR N C
            try {
                long idx = Long.parseLong(c[1]);
                int amount = Integer.parseInt(c[2]);
                String name = c[3];
                int cost = Integer.parseInt(c[4]);
                dao.saveStockItem(new StockItem(idx, name, "", cost, amount));
            } catch (IndexOutOfBoundsException | IllegalArgumentException e) {
                log.error(e.getMessage(), e);
            }
        } else {
            System.out.println("unknown command");
        }
    }
}